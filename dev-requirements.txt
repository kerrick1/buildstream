coverage == 4.4.0
pep8
pylint == 2.1.1
pytest >= 3.8
pytest-cov >= 2.5.0
pytest-datafiles
pytest-env
pytest-pep8
pytest-pylint
pytest-xdist
pytest-timeout
